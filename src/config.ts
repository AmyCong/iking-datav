/*
 * @Author       : wfl
 * @LastEditors: root zhangpengpeng@ikingtech.com
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-10-31 11:13:00
 * @LastEditTime: 2024-03-26 10:40:47
 */

export const globalConfig = {
  title: '金合可视化平台',
  logo: '/datav/file/logo.png',
}
