import { ComDataType } from '@/data/system-components'
const COM_CDN = ''
export const water: ComDataType = {
  type: 'water',
  name: '水球图',
  icon: 'v-icon-other',
  data: [
    {
      name: 'VWaterBall',
      alias: '基础水球图',
      img: `${COM_CDN}/datav/file/com-picture/water-ball-160-116.png`,
      thum: `${COM_CDN}/datav/file/com-picture/water-ball-160-116.png`,
      used: true,
    },
    {
      name: 'VWaterSvg',
      alias: 'SVG水球图',
      img: `${COM_CDN}/datav/file/com-picture/water-svg-160-116.png`,
      thum: `${COM_CDN}/datav/file/com-picture/water-svg-160-116.png`,
      used: true,
    },
  ],
}
