import { mapApi } from '@/api/map'
import { ikStore } from 'iking-utils'

export const useChartMap = async () => {
  const map = await mapApi.mapLists()
  if(location.href.includes('/login')){
    return
  }
  if (map.success) {
    ikStore.local.setItem(ELOCAL_TYPE.地图, map.data)
    return map.data
  } else {
    return []
  }
}
