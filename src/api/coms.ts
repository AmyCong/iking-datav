/*
 * @Author       : wfl
 * @LastEditors: root zhangpengpeng@ikingtech.com
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-09-26 09:52:24
 * @LastEditTime: 2024-03-26 15:04:29
 */
import { netGet, netPost } from '@/utils/request'



export function getComs(screenId: number | string) {
  return netGet('/component/coms', { screenId })
}

export function addCom(data: any) {
  return netPost('/component/add', data)
}

export function addComs(data: any[]) {
  return netPost('/component/adds', data)
}

export function updateComs(data: any[]) {
  return netPost('/component/updates', data)
}

export function deleteComs(ids: string | string[]) {
  return netPost(`/component/del`, ids)
}

export function copyCom(ids: string | string[]) {
  return netPost(`/component/copy`, ids)
}

/**
 * A function that takes the "coms" parameter and sends a POST request to the "/admin/coms/component/collection" endpoint.
 *
 * @param {type} coms - The "coms" parameter is a collection of components.
 * @return {type} The return value is the result of the POST request.
 */
export function collectionComs(coms) {
  return netPost(`/component/collection`, coms)
}


export const collectApi = {
  getCollectComs: () => netGet('/component/list'),
  deleteCollect: ids => netPost('/component/delete/collection', { ids: Array.isArray(ids) ? ids : [ids] }),
  comsPage:params=> netPost('/component/select/page',params),
}
