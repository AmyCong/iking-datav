/*
 * @Author: root zhangpengpeng@ikingtech.com
 * @Date: 2024-03-18 10:47:16
 * @LastEditors: '张朋朋' '14065467+a598001989@user.noreply.gitee.com'
 * @LastEditTime: 2024-04-17 10:52:58
 * @FilePath: \iking-datav-new\src\api\system.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { netPost, netGet } from '@/utils/request'

const textHeader = {
  'Content-Type': 'text/plain',
}
export const userApi = {
  // 获取
  getUserList: async (params: any) => await netPost('/server/system/user/list/page', params),
  // 新增
  addUser: async (params: any) => await netPost('/server/system/user/add', params),
  // 更新
  updateUser: async (params: any) => await netPost('/server/system/user/update', params),
  // 删除
  deleteUsers: async (userId: string) => await netPost('/server/system/user/delete', userId, textHeader),
}

export const deptApi={
  // 所有部门信息
  getAllDeps: async (params?: any) =>
    await netPost('/server/system/dept/info/list/all', params, { 'Content-Type': 'application/json;charset=UTF-8' }),
  // 新增部门
  addDep: async (params: any) => await netPost('/server/system/dept/add', params),
  // 修改
  updateDep: async (params: any) => await netPost('/server/system/dept/update', params),
  // 删除
  deleteDep: async (deptId: string) => await netPost('/server/system/dept/delete', deptId, textHeader),

}

// 获取图片显示配置
export const dispositionApi = {
  getImgUrl:async () => await netGet('/system-config/getFileUrl'),
}

export const typeApi = {
  getList: async (type:string) => await netGet(`/system-config/by/type?type=${type}`),
  edit:async (params:any) => await netPost('/system-config/edit',params),
}
