/*
 * @Author       : wfl
 * @LastEditors  : wfl
 * @description  :
 * @updateInfo   :
 * @Date         : 2023-11-10 18:33:05
 * @LastEditTime : 2024-01-15 18:19:18
 */
import { netPost, netGet, textHeader } from '@/utils/request'

export const connecting = param => netPost('/datasource/schema/list', param)

export const dbApi = {
  addDb: param => netPost('/datasource/add', param),
  queryDb: (param:any) => netPost('/datasource/list/page',param),
  updateDb: param => netPost('/datasource/update', param),
  deleteDb: id => netPost(`/datasource/delete`, id , textHeader),
  queryDBBases: (param : any) => netPost('/datasource/schema/list', param),
  queryDBTables: param => netPost('/datasource/table/list', param),
  queryDBTableKeys: param => netPost('/datasource/field/list', param),
}
