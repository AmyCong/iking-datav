import { netGet, netPost ,textHeader } from '@/utils/request'

const dictApi = {
  getDictTypes: async (id:string) => await netPost('/server/system/dict/item/list/dict-id',id,textHeader),
  getDictValues: async (types: string[]) => await netPost('/admin/dict/info/data', { types }),
}

export default dictApi
