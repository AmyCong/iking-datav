/*
 * @Author       : wfl
 * @LastEditors: root zhangpengpeng@ikingtech.com
 * @description  :
 * @updateInfo   :
 * @Date         : 2024-01-08 09:33:53
 * @LastEditTime: 2024-03-26 14:09:32
 */

import { defineStore } from 'pinia'
import dictApi from '@/api/dict'
import { ikTree } from 'iking-utils'
type TDict = {
  id: string
  name: string
  value: string
}
interface IDictState {
  fontMamilys: TDict[]
  numberFormats: TDict[]
  dateFormats: TDict[]
}

export const useDictStore = defineStore('dict', {
  state: (): IDictState => ({
    fontMamilys: [],
    numberFormats: [],
    dateFormats: [],
  }),
  getters: {},
  actions: {
    async requestDicts() {
      if(location.href.includes('/login')){
        return
      }
      const { success,data,msg } = await dictApi.getDictTypes('f1df617a9c124efca4dd6fc9617261fd')
      if (success) {
        const fa:Set<string> = new Set(
          data.map((item:{dictCode: string;})=>item?.dictCode),
        )
        for (const i of fa){
          data.push({ id:i,name:i })
        }
        const dict:{[key:string]:any;} = {}
        const tree = ikTree.listToTree(data,{ id:'id',pid:'dictCode' })
        for (const i of fa){
          dict[i] = tree.find((f:{id:string;})=>f.id===i)?.children
        }
      }else{
        console.error(msg)
      }
    },
  },
})
